# Automate cleanup of old Snapshots with Python

#### Project Outline

Following on from the project 

[Backup EC2 Instance Volumes with Python Repository](https://gitlab.com/FM1995/backup-ec2-instance-volumes-with-python)

Despite taking a number of snapshots which we can recover from an EC2 outage, having unnecessary amount of snapshots can reduce storage and can increase our bill. To reduce the impact of this we can modify our script to have only the most recent snapshots therefore taking away all the older snapshots and therefore reduce storage costs. We can also implement in our script to do a cleanup every month, once a week or once a day.

#### Lets get started

Checking the old snapshots

![Image1](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image1.png)

Adding new file ‘cleanup-snapshots.py’

![Image2](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image2.png)

Using the below doc

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html

First will define the client

![Image3](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image3.png)

![Image4](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image4.png)

Now using the below documentation we can list the snapshots

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_snapshots.html

Now there are snapshots which aws creates as opposed to the one’s we have created, to separate the snapshots can use the attribute called owner id as listed below

Hence will go with the ‘self’

![Image5](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image5.png)

Can then implement that

![Image6](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image6.png)

Can then look at the response

![Image7](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image7.png)


And then grab it and implement it, which displays the results

![Image8](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image8.png)

Can then go through the snapshots and find the creation times

Can then check the attribute start time

![Image9](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image9.png)

We want to implement a logic which takes the latest snapshots, similar to the sort feature in the console

![Image10](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image10.png)

Can then introduce the sorted function which sorts it by StartTime, but before that we will have to import the operator called itemgetter

![Image11](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image11.png)

Can introduce the sorted function

![Image12](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image12.png)

Can then print out the StartTime from the Snapshots

![Image13](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image13.png)

Then print out the sorted lists with both the unsorted and sorted

![Image14](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image14.png)

Can use the ‘reverse=True’ attribute to go from the most recent one to the oldest one

![Image15](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image15.png)

Now what we want to do is keep the latest two in our AWS account and delete the rest

We can implement the function delete snapshot

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/delete_snapshot.html

![Image16](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image16.png)

Can then implement the logic which will delete snapshot displayed from the third one hence [2]

![Image17](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image17.png)

Now below is our logic Fetch our Snapshots created by ourselves and not AWS -> Sort the snapshots in the descending order where we have the most recent first -> Delete the rest of the snapshots apart from the latest two

Below is the finalised code

![Image18](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image18.png)

And below is what we get when we execute our code

![Image19](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image19.png)

And can see in our console we are left with two snapshots as oppose to four which we had earlier

![Image20](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image20.png)

Now we can implement a scheduler to execute this at a particular time. We can achieve it using the below steps.

First import schedule, then Define a function delete_old_snapshots() that retrieves all snapshots, sorts them by creation date, and deletes all snapshots except for the two most recent ones.

Then Use the schedule library to schedule the delete_old_snapshots() function to run every day at midnight.

Start the scheduling loop using the while True loop.

![Image21](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image21.png)

However the above applies to snapshots regardless of tag or environment perhaps

We can modify our code further and filter for only ‘prod’ volume snapshots and do clean up of those to have only the two most recent ones. We can implement the below

•	Fetch Volumes Tagged 'prod':

•	Retrieves information about EC2 volumes tagged as 'prod' using describe_volumes.

•	Iterate Through 'prod' Tagged Volumes:

•	Loops through each 'prod' tagged volume obtained in step 1 using for volume in volumes['Volumes']:

•	Fetch Snapshots Associated with Each Volume:

•	For each 'prod' tagged volume:

•	Retrieves information about snapshots associated with that specific volume using describe_snapshots.

•	Filters snapshots based on the 'volume-id', ensuring only snapshots linked to the current volume are obtained.

•	Limits the snapshots fetched to those owned by your AWS account ('self').


![Image22](https://gitlab.com/FM1995/automate-cleanup-of-old-snapshots-with-python/-/raw/main/Images/Image22.png)








